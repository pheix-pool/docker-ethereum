FROM buildpack-deps:22.04-scm
LABEL org.opencontainers.image.authors="konstantin@narkhov.pro"

ENV DEVLOCALPATH=/ethereum-local-network/utils
ENV GOPATH=/go

ENV GOLANG_VERSION=1.22.12
ENV SOLIDITY_VERSION=0.8.28
ENV GOETHEREUM_VERSION=1.13.15
ENV Z3_VERSION=4.12.1

ENV PATH=/go-ethereum/build/bin:/usr/local/bin:$GOPATH/bin:/usr/local/go/bin:$PATH:

RUN apt-get update && apt-get install -y --no-install-recommends \
		g++ \
		gcc \
		libc6-dev \
		make \
		pkg-config \
		sudo \
		cmake \
		bc \
		mc \
		screen \
		vim \
		jq \
		lsb-release \
		build-essential \
		libboost-all-dev \
		yasm \
		libudev-dev \
		clang \
		llvm \
		llvm-dev \
		python-is-python3 \
		netcat \
	&& rm -rf /var/lib/apt/lists/*; \
	set -eux; \
	now="$(date '+%s')"; \
	arch="$(dpkg --print-architecture)"; arch="${arch##*-}"; \
	url=; \
	case "$arch" in \
		'amd64') \
			url='https://dl.google.com/go/go1.22.12.linux-amd64.tar.gz'; \
			sha256='4fa4f869b0f7fc6bb1eb2660e74657fbf04cdd290b5aef905585c86051b34d43'; \
			;; \
		'armhf') \
			url='https://dl.google.com/go/go1.22.12.linux-armv6l.tar.gz'; \
			sha256='bcd678461bb74cda217fb5aa3cc914b2021be6d828f0c6fb4e3a36c3d7312acb'; \
			;; \
		'arm64') \
			url='https://dl.google.com/go/go1.22.12.linux-arm64.tar.gz'; \
			sha256='fd017e647ec28525e86ae8203236e0653242722a7436929b1f775744e26278e7'; \
			;; \
		'i386') \
			url='https://dl.google.com/go/go1.22.12.linux-386.tar.gz'; \
			sha256='40d4c297bc2e964e9c96fe79bb323dce79b77b8b103fc7cc52e0a87c7849890f'; \
			;; \
		'mips64el') \
			url='https://dl.google.com/go/go1.22.12.linux-mips64le.tar.gz'; \
			sha256='2d473895f9c1dc8c86d51eb13f8ca49b7eea46010759fd71efed3eecacf5335b'; \
			;; \
		'ppc64el') \
			url='https://dl.google.com/go/go1.22.12.linux-ppc64le.tar.gz'; \
			sha256='9573d30003b0796717a99d9e2e96c48fddd4fc0f29d840f212c503b03d7de112'; \
			;; \
		'riscv64') \
			url='https://dl.google.com/go/go1.22.12.linux-riscv64.tar.gz'; \
			sha256='f03a084aabc812fdc15b29acd5e1ee18e13b3c80be22aec43990119afcaf4947'; \
			;; \
		's390x') \
			url='https://dl.google.com/go/go1.22.12.linux-s390x.tar.gz'; \
			sha256='e1b20935cc790fdc4c48c0e3e6dd11be57ac09e4eb302ba2cdf146276468b346'; \
			;; \
		*) echo >&2 "error: unsupported architecture '$arch' (likely packaging update needed)"; exit 1 ;; \
	esac; \
	\
	wget -O go.tgz.asc "$url.asc"; \
	wget -O go.tgz "$url" --progress=dot:giga; \
	echo "$sha256 *go.tgz" | sha256sum -c -; \
	\
# https://github.com/golang/go/issues/14739#issuecomment-324767697
	GNUPGHOME="$(mktemp -d)"; export GNUPGHOME; \
# https://www.google.com/linuxrepositories/
	gpg --batch --keyserver keyserver.ubuntu.com --recv-keys 'EB4C 1BFD 4F04 2F6D DDCC  EC91 7721 F63B D38B 4796'; \
# let's also fetch the specific subkey of that key explicitly that we expect "go.tgz.asc" to be signed by, just to make sure we definitely have it
	gpg --batch --keyserver keyserver.ubuntu.com --recv-keys '2F52 8D36 D67B 69ED F998  D857 78BD 6547 3CB3 BD13'; \
	gpg --batch --verify go.tgz.asc go.tgz; \
	gpgconf --kill all; \
	rm -rf "$GNUPGHOME" go.tgz.asc; \
	\
	tar -C /usr/local -xzf go.tgz; \
	rm go.tgz; \
	\
# save the timestamp from the tarball so we can restore it for reproducibility, if necessary (see below)
	SOURCE_DATE_EPOCH="$(stat -c '%Y' /usr/local/go)"; \
	export SOURCE_DATE_EPOCH; \
	touchy="$(date -d "@$SOURCE_DATE_EPOCH" '+%Y%m%d%H%M.%S')"; \
# for logging validation/edification
	date --date "@$SOURCE_DATE_EPOCH" --rfc-2822; \
# sanity check (detected value should be older than our wall clock)
	[ "$SOURCE_DATE_EPOCH" -lt "$now" ]; \
	\
	if [ "$arch" = 'armhf' ]; then \
		[ -s /usr/local/go/go.env ]; \
		before="$(go env GOARM)"; [ "$before" != '7' ]; \
		{ \
			echo; \
			echo '# https://github.com/docker-library/golang/issues/494'; \
			echo 'GOARM=7'; \
		} >> /usr/local/go/go.env; \
		after="$(go env GOARM)"; [ "$after" = '7' ]; \
# (re-)clamp timestamp for reproducibility (allows "COPY --link" to be more clever/useful)
		touch -t "$touchy" /usr/local/go/go.env /usr/local/go; \
	fi; \
	\
	mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"; \
	cd /; \
	\
# install Z3 prover
	cd /; \
	git clone https://github.com/Z3Prover/z3.git; \
	cd z3; \
	git checkout z3-${Z3_VERSION}; \
	python scripts/mk_make.py; \
	cd build; \
	make; \
	make install; \
	\
# install solidity
	cd /; \
	mkdir $HOME/solidity; \
	curl -fsSL https://github.com/ethereum/solidity/releases/download/v${SOLIDITY_VERSION}/solidity_${SOLIDITY_VERSION}.tar.gz -o solidity.tar.gz; \
	tar xzf solidity.tar.gz --strip-components=1 -C $HOME/solidity; \
	cd $HOME/solidity; \
	./scripts/build.sh; \
	\
# install go-ethereum
	cd /; \
	git clone https://github.com/ethereum/go-ethereum.git; \
	cd /go-ethereum; \
	git checkout v${GOETHEREUM_VERSION}; \
	make all; \
	\
# install local private network sample
	cd /; \
	git clone https://gitlab.com/pheix-research/ethereum-local-network.git; \
	cd /ethereum-local-network/geth.local; \
	./reinit.sh; \
	\
# import smart contacts
	cd /; \
	git clone https://gitlab.com/pheix-research/smart-contracts.git; \
	cd /smart-contracts/t; \
	./compile-contract.sh StoreBytesArray --tgz; \
	./compile-contract.sh BigBro --tgz; \
	./compile-contract.sh PheixDatabase --tgz; \
	./compile-contract.sh PheixAuth --tgz; \
	\
# clean up
	rm -rf /solidity.tar.gz $HOME/solidity; \
	apt-get purge -y make cmake gcc g++ pkg-config; \
	dpkg -l | grep -E "\-dev\s" | grep -v "ssl" | awk '{print $2}' | xargs -d '\n' -- apt-get purge -y; \
	apt-get clean; \
	\
# check versions
	z3 --version; \
	solc --version; \
	geth version; \
	jq --version; \
	vi --version;

WORKDIR $DEVLOCALPATH
