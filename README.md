# Ethereum Docker image

This Docker image includes Go, Go-Ethereum suite, Solidity compiler, [ethereum private network sample](https://gitlab.com/pheix-research/ethereum-local-network) and [smart contract suite](https://gitlab.com/pheix-research/smart-contracts).

# Build

You can build an image from this Dockerfile :

```bash
$ sudo docker build -t ethereum-image /path_to_dockerfile/
```

Run your newly built image:

```bash
$ sudo docker run -it ethereum-image
root@cc715632ef40:/#
```

You can mount a directory from the host within a container:

```bash
$ sudo docker run -it -v $HOME/:/mount_location/ ethereum-image /bin/bash
```

# Usage

You can pull pre-built docker image from my registry and start development of your decentralized apps right now:

1. Pull the image:

```bash
sudo docker pull registry.gitlab.com/pheix-docker/go-ethereum
```

2. Create an alias for pulled image:

```bash
sudo docker tag registry.gitlab.com/pheix-docker/go-ethereum ethereum-dev
```

3. Run this image with bridged network from container to host:

```bash
sudo docker run --net=host --name=ethereum-dev -it ethereum-dev
```

4. Start local private ethereum network:

```bash
root@localhost:/ethereum-local-network/geth.local# cd utils
root@localhost:/ethereum-local-network/utils# ./run-geth.sh
```

5. Create a new console to running docker container.

```bash
sudo docker exec -it ethereum-dev bash
```

6. Attach to first ethereum network node:

```bash
root@localhost:/ethereum-local-network/geth.local# bash attach.node0
```

7. You' have got local private ethereum network available on your host machine: first node at http://127.0.0.1:8540, second node at: http://127.0.0.1:8541.

# Z3 solver version

Solidity compiler depends on Z3 solver version, this dependence is referred at `CMakeLists.txt`: https://github.com/ethereum/solidity/blob/develop/CMakeLists.txt#L97.

Details: https://github.com/ethereum/solidity/issues/13178#issuecomment-1342972287

# License information

This is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

# Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
